var app = new Framework7({
    root: '#app',
    name: 'My App',
    id: 'com.somospnt.framework7.demo',
    view: {
        pushState: true
    },
    panel: {
        swipe: 'left',
    },
    routes: [
        {
            path: '/simple',
            url: './views/simple.html',
        },
        {
            path: '/paginaComoTemplate',
            templateUrl: './views/paginaComoTemplate.html',
            options: {
                context: crearContexto
            }
        },
        {
            path: '/paginaComoTemplateAsync',
            async: function (routeTo, routeFrom, resolve, reject) {
                setTimeout(function () {
                    resolve({templateUrl: './views/paginaComoTemplateAsync.html'}, {context: crearContexto});
                }, 1000);
//                resolve({templateUrl: './views/paginaComoTemplateAsync.html'}, {context: crearContexto});
            }
        }
    ]
});

var mainView = app.views.create('.view-main');

setTimeout(function () {
    //inicalizar app despues de una función larga, etc...
    mainView.router.navigate("/simple", {reloadCurrent: true});
}, 100);


function crearContexto() {
    return {
        personajes: [
            {nombre: "Jerry", serie: "Seinfeld"},
            {nombre: "Kramer", serie: "Seinfeld"},
            {nombre: "Ross", serie: "Friends"},
            {nombre: "Monica", serie: "Friends"},
            {nombre: "Zim", serie: "Invader Zim"},
            {nombre: "Gaz", serie: "Invader Zim"},
            {nombre: "Walter White", serie: "Breaking Bad"},
            {nombre: "Jessy Pinkman", serie: "Breaking Bad"}
        ]
    };
}